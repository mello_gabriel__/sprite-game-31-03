﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScript : MonoBehaviour
{
 
    private float screenWidth;

    void Start()
    {
        
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();

        // Tamanho da imagem
        float imageWidth = renderer.sprite.bounds.size.x;
        float imageHeight = renderer.sprite.bounds.size.y;

        // Tamanho da tela
        float screenHeight = Camera.main.orthographicSize * 3.75f;
        screenWidth = screenHeight / Screen.height * Screen.width;

        // Tamanho da imagem para a tela
        Vector2 newScale = this.transform.localScale;

        newScale.x = screenWidth / imageWidth + 0.25f;
        newScale.x = screenHeight / imageHeight;

        this.transform.localScale = newScale;

    }

    void Update()
    {
        
    }
}
