﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomemAcao : MonoBehaviour
{
    public float velocity, jumpForce;
    private bool jump;
    private Rigidbody2D body;
    private SpriteRenderer render;
    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        this.jump = false;
        
        this.body = GetComponent<Rigidbody2D>();
        this.render = GetComponent<SpriteRenderer>();
        this.animator = GetComponent<Animator>();

        this.animator.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        float x = this.transform.position.x;

        float y = this.transform.position.y;

        // Para frente e tras
        float inputKeyHorizontal = Input.GetAxis("Horizontal");

        // Para cima e baixo
        float inputKeyVertical = Input.GetAxis("Vertical");

        if (inputKeyHorizontal > 0)
        {
            this.body.AddForce(new Vector2(this.velocity, 0));
            this.animator.enabled = true;
            this.render.flipX = false;
        }
        else if (inputKeyHorizontal < 0)
        {
            this.body.AddForce(new Vector2(-this.velocity, 0));
            this.animator.enabled = true;
            this.render.flipX = true;
        }
        else
        {
            this.body.MovePosition(new Vector2(x, y));
            this.animator.enabled = false;
        }
        // Pulo
        if (Input.GetButton("Jump") && !this.jump)
        {
            this.jump = true;
            this.body.AddForce(new Vector2(0, this.jumpForce));
            this.animator.enabled = true;
        }
        else if (this.body.velocity.y == 0 && this.jump)
        {
            this.jump = false;
            this.animator.enabled = true;
        }
    }
}
